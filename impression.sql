-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 16, 2018 at 10:26 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `impression`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_query`
--

CREATE TABLE `ci_query` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `query_string` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_query`
--

INSERT INTO `ci_query` (`id`, `query_string`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `log_access`
--

CREATE TABLE `log_access` (
  `access_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `status` enum('login','logout') NOT NULL DEFAULT 'login',
  `create_date` datetime NOT NULL,
  `create_ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `log_access`
--

INSERT INTO `log_access` (`access_id`, `user_id`, `user_group_id`, `status`, `create_date`, `create_ip`) VALUES
(1, 1, 0, 'login', '2018-12-14 10:42:29', '::1'),
(2, 1, 0, 'login', '2018-12-14 17:28:13', '::1'),
(3, 1, 0, 'login', '2018-12-15 16:19:37', '::1'),
(4, 1, 0, 'login', '2018-12-15 21:04:42', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `log_action`
--

CREATE TABLE `log_action` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `table_name` char(30) NOT NULL,
  `content_id` bigint(20) NOT NULL,
  `action_name` varchar(255) NOT NULL,
  `action_detail` text NOT NULL,
  `action_query` text NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `log_action`
--

INSERT INTO `log_action` (`action_id`, `table_name`, `content_id`, `action_name`, `action_detail`, `action_query`, `create_by`, `create_date`, `create_ip`) VALUES
(1, 'mother_config', 31, 'create config', 'model : ConfigModel<br />config_group_id : 2<br />config_name : Google Map API Key<br />config_code : google_map_api_key<br />config_field_type : text<br />sort_priority : 11<br />config_type : content', 'INSERT INTO `mother_config` (`config_group_id`, `config_name`, `config_code`, `config_field_type`, `sort_priority`, `config_type`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'2\', \'Google Map API Key\', \'google_map_api_key\', \'text\', \'11\', \'content\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-14 10:48:34', '::1'),
(2, 'mother_column', 1, 'create column', 'model : StructureModel<br />column_name : First name<br />column_code : first_name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 2', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'First name\', \'first_name\', \'true\', \'text\', \'0\', \'0\', \'1\', \'Disable\', \'\', \'\', \'1\', \'2\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-14 10:49:54', '::1'),
(3, 'mother_column', 2, 'create column', 'model : StructureModel<br />column_name : Last name<br />column_code : last_name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 2', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'Last name\', \'last_name\', \'true\', \'text\', \'0\', \'0\', \'1\', \'Disable\', \'\', \'\', \'2\', \'2\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-14 10:50:25', '::1'),
(4, 'mother_column', 3, 'create column', 'model : StructureModel<br />column_name : Email<br />column_code : email<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 3<br />table_id : 2', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'Email\', \'email\', \'false\', \'text\', \'0\', \'0\', \'1\', \'Disable\', \'\', \'\', \'3\', \'2\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-14 10:50:56', '::1'),
(5, 'mother_column', 4, 'create column', 'model : StructureModel<br />column_name : Mobile<br />column_code : mobile<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 4<br />table_id : 2', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'Mobile\', \'mobile\', \'false\', \'text\', \'0\', \'0\', \'1\', \'Disable\', \'\', \'\', \'4\', \'2\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-14 10:51:25', '::1'),
(6, 'mother_column', 5, 'create column', 'model : StructureModel<br />column_name : Room Type<br />column_code : room_type<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 5<br />table_id : 2', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'Room Type\', \'room_type\', \'false\', \'text\', \'0\', \'0\', \'0\', \'Disable\', \'\', \'\', \'5\', \'2\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-14 10:51:52', '::1'),
(7, 'mother_column', 6, 'create column', 'model : StructureModel<br />column_name : Price range<br />column_code : price_range<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 6<br />table_id : 2', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'Price range\', \'price_range\', \'false\', \'text\', \'0\', \'0\', \'0\', \'Disable\', \'\', \'\', \'6\', \'2\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-14 10:52:23', '::1'),
(8, 'mother_column', 7, 'create column', 'model : StructureModel<br />column_name : UTM Source<br />column_code : utm_source<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 7<br />table_id : 2', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'UTM Source\', \'utm_source\', \'false\', \'text\', \'0\', \'0\', \'0\', \'Disable\', \'\', \'\', \'7\', \'2\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-14 10:53:14', '::1'),
(9, 'mother_config', 32, 'create config', 'model : ConfigModel<br />config_group_id : 2<br />config_name : Google Map<br />config_code : google_map<br />config_field_type : text<br />sort_priority : 12<br />config_type : content', 'INSERT INTO `mother_config` (`config_group_id`, `config_name`, `config_code`, `config_field_type`, `sort_priority`, `config_type`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'2\', \'Google Map\', \'google_map\', \'text\', \'12\', \'content\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-14 18:06:09', '::1'),
(10, 'mother_table', 3, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : Management<br />table_code : management<br />table_type : static<br />table_order : asc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />show_dashboard : false<br />icon_id : 1<br />sort_priority : 2', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `show_dashboard`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'0\', \'Management\', \'management\', \'static\', \'asc\', \'true\', \'false\', \'\', \'false\', \'1\', \'2\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-15 16:20:46', '::1'),
(11, 'mother_column', 8, 'create column', 'model : StructureModel<br />column_name : Promotion<br />column_code : promotion<br />column_main : true<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 3', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'Promotion\', \'promotion\', \'true\', \'image\', \'0\', \'0\', \'1\', \'Disable\', \'\', \'\', \'1\', \'3\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-15 16:21:08', '::1'),
(12, 'mother_column', 9, 'create column', 'model : StructureModel<br />column_name : Thank You<br />column_code : thank_you<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 3', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'Thank You\', \'thank_you\', \'false\', \'text\', \'0\', \'0\', \'0\', \'Disable\', \'\', \'\', \'2\', \'3\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-15 16:21:42', '::1'),
(13, 'mother_column', 9, 'delete column', 'model : StructureModel<br />column_id : 9<br />column_field_type : text => image', 'UPDATE `mother_column` SET `column_field_type` = \'image\', `update_date` = NOW(), `update_by` = \'1\' WHERE `column_id` =  \'9\'', 1, '2018-12-15 16:21:59', '::1'),
(14, 'mother_column', 9, 'delete column', 'model : StructureModel<br />column_id : 9<br />column_code : thank_you => thankyou', 'UPDATE `mother_column` SET `column_code` = \'thankyou\', `update_date` = NOW(), `update_by` = \'1\' WHERE `column_id` =  \'9\'', 1, '2018-12-15 16:22:12', '::1'),
(15, 'mother_column', 9, 'delete column', 'model : StructureModel<br />column_id : 9<br />column_name : Thank You => Thank You PC<br />column_code : thankyou => thankyou_pc', 'UPDATE `mother_column` SET `column_name` = \'Thank You PC\', `column_code` = \'thankyou_pc\', `update_date` = NOW(), `update_by` = \'1\' WHERE `column_id` =  \'9\'', 1, '2018-12-15 16:23:15', '::1'),
(16, 'mother_column', 10, 'create column', 'model : StructureModel<br />column_name : Thank You Mobile<br />column_code : thankyou_mobile<br />column_main : false<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 3<br />table_id : 3', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (\'Thank You Mobile\', \'thankyou_mobile\', \'false\', \'image\', \'0\', \'0\', \'0\', \'Disable\', \'\', \'\', \'3\', \'3\', NOW(), \'1\', NOW(), \'1\')', 1, '2018-12-15 16:23:41', '::1'),
(17, 'tbl_management', 1, 'insert Management', 'model : BackendModel<br />promotion : /userfiles/images/promotion(1).svg<br />thankyou_pc : /userfiles/images/thankyou.svg<br />thankyou_mobile : /userfiles/images/thankyou_mobile.svg<br />recursive_id : 0', 'INSERT INTO `tbl_management` (`promotion`, `thankyou_pc`, `thankyou_mobile`, `recursive_id`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (\'/userfiles/images/promotion(1).svg\', \'/userfiles/images/thankyou.svg\', \'/userfiles/images/thankyou_mobile.svg\', 0, NOW(), \'1\', NOW(), \'1\', \'1\')', 1, '2018-12-15 16:25:39', '::1'),
(18, 'tbl_management_lang', 1, 'insert Management_lang', 'model : BackendModel', 'INSERT INTO `tbl_management_lang` (`management_id`, `lang_id`) VALUES (\'1\', \'1\')', 1, '2018-12-15 16:25:39', '::1'),
(19, 'tbl_management_lang', 1, 'insert Management_lang', 'model : BackendModel', 'INSERT INTO `tbl_management_lang` (`management_id`, `lang_id`) VALUES (\'1\', \'2\')', 1, '2018-12-15 16:25:39', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(16);

-- --------------------------------------------------------

--
-- Table structure for table `mother_column`
--

CREATE TABLE `mother_column` (
  `column_id` int(11) UNSIGNED NOT NULL,
  `table_id` int(11) NOT NULL,
  `column_name` varchar(50) NOT NULL,
  `column_code` varchar(50) NOT NULL,
  `column_main` enum('true','false') NOT NULL DEFAULT 'false',
  `column_field_type` varchar(50) NOT NULL,
  `column_lang` tinyint(4) NOT NULL,
  `column_show_list` tinyint(4) NOT NULL,
  `column_option` varchar(255) NOT NULL,
  `column_relation_table` int(11) NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `searchable` enum('Disable','Normal Search','Advance Search') NOT NULL,
  `column_remark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mother_column`
--

INSERT INTO `mother_column` (`column_id`, `table_id`, `column_name`, `column_code`, `column_main`, `column_field_type`, `column_lang`, `column_show_list`, `column_option`, `column_relation_table`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`, `searchable`, `column_remark`) VALUES
(1, 2, 'First name', 'first_name', 'false', 'text', 0, 1, '', 0, 1, '2018-12-14 10:49:54', 1, '2018-12-14 10:49:54', 1, 'Disable', ''),
(2, 2, 'Last name', 'last_name', 'true', 'text', 0, 1, '', 0, 2, '2018-12-14 10:50:25', 1, '2018-12-14 10:50:25', 1, 'Disable', ''),
(3, 2, 'Email', 'email', 'false', 'text', 0, 1, '', 0, 3, '2018-12-14 10:50:56', 1, '2018-12-14 10:50:56', 1, 'Disable', ''),
(4, 2, 'Mobile', 'mobile', 'false', 'text', 0, 1, '', 0, 4, '2018-12-14 10:51:25', 1, '2018-12-14 10:51:25', 1, 'Disable', ''),
(5, 2, 'Room Type', 'room_type', 'false', 'text', 0, 0, '', 0, 5, '2018-12-14 10:51:52', 1, '2018-12-14 10:51:52', 1, 'Disable', ''),
(6, 2, 'Price range', 'price_range', 'false', 'text', 0, 0, '', 0, 6, '2018-12-14 10:52:23', 1, '2018-12-14 10:52:23', 1, 'Disable', ''),
(7, 2, 'UTM Source', 'utm_source', 'false', 'text', 0, 0, '', 0, 7, '2018-12-14 10:53:14', 1, '2018-12-14 10:53:14', 1, 'Disable', ''),
(8, 3, 'Promotion', 'promotion', 'true', 'image', 0, 1, '', 0, 1, '2018-12-15 16:21:08', 1, '2018-12-15 16:21:08', 1, 'Disable', ''),
(9, 3, 'Thank You PC', 'thankyou_pc', 'false', 'image', 0, 0, '', 0, 2, '2018-12-15 16:21:42', 1, '2018-12-15 16:23:15', 1, 'Disable', ''),
(10, 3, 'Thank You Mobile', 'thankyou_mobile', 'false', 'image', 0, 0, '', 0, 3, '2018-12-15 16:23:41', 1, '2018-12-15 16:23:41', 1, 'Disable', '');

-- --------------------------------------------------------

--
-- Table structure for table `mother_config`
--

CREATE TABLE `mother_config` (
  `config_id` int(11) UNSIGNED NOT NULL,
  `config_name` varchar(50) NOT NULL,
  `config_code` varchar(50) NOT NULL,
  `config_type` enum('content','admin') NOT NULL DEFAULT 'content',
  `config_field_type` varchar(50) NOT NULL,
  `config_value` text NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `config_group_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mother_config`
--

INSERT INTO `mother_config` (`config_id`, `config_name`, `config_code`, `config_type`, `config_field_type`, `config_value`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`, `config_group_id`) VALUES
(1, 'Backend Title', 'site_title', 'admin', 'text', 'Backend', 1, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 1),
(2, 'Navigate Color', 'navigate_color', 'admin', 'color', '#454545', 2, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 1),
(3, 'Navigate Shadow Color', 'navigate_color_shadow', 'admin', 'color', '#1f1f1f', 3, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 1),
(4, 'Navigate Color Active', 'navigate_color_active', 'admin', 'color', '#6b6b6b', 4, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 1),
(5, 'Website Title', 'website_title', 'admin', 'text', '', 1, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 2),
(6, 'Favicon', 'favicon', 'admin', 'file', '', 2, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 2),
(7, 'Meta Keywords', 'meta_keywords', 'admin', 'textarea', '', 3, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 2),
(8, 'Meta Description', 'meta_description', 'admin', 'textarea', '', 4, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 2),
(9, 'OG Title', 'og_title', 'admin', 'text', '', 5, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 2),
(10, 'OG Description', 'og_description', 'admin', 'text', '', 6, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 2),
(11, 'OG Sitename', 'og_sitename', 'admin', 'text', '', 7, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 2),
(12, 'OG Type', 'og_type', 'admin', 'text', '', 8, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 2),
(13, 'OG Url', 'og_url', 'admin', 'text', '', 9, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 2),
(14, 'OG Image', 'og_image', 'admin', 'file', '', 10, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 2),
(15, 'Email Account', 'email_account', 'admin', 'text', '', 1, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 3),
(16, 'Password', 'password', 'admin', 'text', '', 2, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 3),
(17, 'SMTP', 'smtp', 'admin', 'text', '', 3, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 3),
(18, 'Port', 'port', 'admin', 'text', '', 4, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 3),
(19, 'To', 'to', 'admin', 'textarea', '', 5, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 3),
(20, 'CC', 'cc', 'admin', 'textarea', '', 8, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 3),
(21, 'BCC', 'bcc', 'admin', 'textarea', '', 9, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 3),
(22, 'Google Analytic', 'google_analytic', 'admin', 'textarea', '', 1, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 4),
(23, 'Google Remarketing', 'google_remarketing', 'admin', 'textarea', '', 2, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0, 4),
(24, 'Google Meta', 'google_meta', 'admin', 'text', '', 5, '2018-12-14 10:42:16', 0, '2018-12-14 10:42:16', 0, 2),
(25, 'From', 'from', 'admin', 'text', '', 6, '2018-12-14 10:42:16', 0, '2018-12-14 10:42:16', 0, 3),
(26, 'From Name', 'from_name', 'admin', 'text', '', 7, '2018-12-14 10:42:16', 0, '2018-12-14 10:42:16', 0, 3),
(27, 'SMS', 'sms', 'admin', 'text', '', 9, '2018-12-14 10:42:16', 0, '2018-12-14 10:42:16', 0, 3),
(28, 'SMS User', 'sms_user', 'admin', 'text', '', 10, '2018-12-14 10:42:16', 0, '2018-12-14 10:42:16', 0, 3),
(29, 'SMS Password', 'sms_password', 'admin', 'text', '', 11, '2018-12-14 10:42:16', 0, '2018-12-14 10:42:16', 0, 3),
(30, 'SMS Sender', 'sms_sender', 'admin', 'text', '', 12, '2018-12-14 10:42:16', 0, '2018-12-14 10:42:16', 0, 3),
(31, 'Google Map API Key', 'google_map_api_key', 'content', 'text', 'AIzaSyBoi2HtTHepEz1uFmvI-gKVV5OZiewqPI8', 11, '2018-12-14 10:48:34', 1, '2018-12-14 10:48:34', 1, 2),
(32, 'Google Map', 'google_map', 'content', 'text', '13.716102, 100.662562', 12, '2018-12-14 18:06:09', 1, '2018-12-14 18:06:09', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `mother_config_group`
--

CREATE TABLE `mother_config_group` (
  `config_group_id` int(11) UNSIGNED NOT NULL,
  `config_group_name` varchar(50) NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mother_config_group`
--

INSERT INTO `mother_config_group` (`config_group_id`, `config_group_name`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Backend Setting', 1, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0),
(2, 'Frontend Setting', 2, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0),
(3, 'Email Setting', 3, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0),
(4, 'Statistics Setting', 4, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mother_icon`
--

CREATE TABLE `mother_icon` (
  `icon_id` int(11) UNSIGNED NOT NULL,
  `icon_name` varchar(50) NOT NULL,
  `icon_image` varchar(255) NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mother_icon`
--

INSERT INTO `mother_icon` (`icon_id`, `icon_name`, `icon_image`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Concept', '/userfiles/images/admin_icon/concept.png', 1, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0),
(2, 'News', '/userfiles/images/admin_icon/news.png', 2, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0),
(3, 'Contact', '/userfiles/images/admin_icon/contact.png', 3, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0),
(4, 'Facility', '/userfiles/images/admin_icon/facility.png', 4, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0),
(5, 'Floor Plan', '/userfiles/images/admin_icon/floorplan.png', 5, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0),
(6, 'Gallery', '/userfiles/images/admin_icon/gallery.png', 6, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0),
(7, 'Register', '/userfiles/images/admin_icon/register.png', 7, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0),
(8, 'Room type', '/userfiles/images/admin_icon/roomtype.png', 8, '2018-12-14 10:42:15', 0, '2018-12-14 10:42:15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mother_lang`
--

CREATE TABLE `mother_lang` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `lang_name` varchar(50) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mother_lang`
--

INSERT INTO `mother_lang` (`lang_id`, `lang_name`, `lang_code`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Thai', 'th', 1, '2013-11-21 15:50:33', 0, '2013-11-21 15:50:33', 0),
(2, 'English', 'english', 2, '2013-11-27 09:49:48', 1, '2013-11-27 09:49:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mother_permission`
--

CREATE TABLE `mother_permission` (
  `permission_id` int(11) UNSIGNED NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `table_id` varchar(50) NOT NULL,
  `view` enum('true','false') NOT NULL DEFAULT 'false',
  `edit` enum('true','false') NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mother_shop`
--

CREATE TABLE `mother_shop` (
  `shop_id` int(11) UNSIGNED NOT NULL,
  `shop_name` varchar(50) NOT NULL,
  `shop_code` varchar(50) NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mother_shop`
--

INSERT INTO `mother_shop` (`shop_id`, `shop_name`, `shop_code`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Main', 'main', 1, '2013-11-21 15:50:33', 0, '2013-11-21 15:50:33', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mother_table`
--

CREATE TABLE `mother_table` (
  `table_id` int(11) UNSIGNED NOT NULL,
  `parent_table_id` int(11) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `table_code` varchar(255) NOT NULL,
  `table_type` enum('static','dynamic') NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `icon_id` int(11) NOT NULL DEFAULT '0',
  `table_order` enum('asc','desc') NOT NULL DEFAULT 'asc',
  `table_newcontent` enum('true','false') NOT NULL DEFAULT 'true',
  `table_preview` text NOT NULL,
  `table_recursive` enum('true','false') NOT NULL DEFAULT 'false',
  `show_dashboard` enum('true','false') NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mother_table`
--

INSERT INTO `mother_table` (`table_id`, `parent_table_id`, `table_name`, `table_code`, `table_type`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`, `icon_id`, `table_order`, `table_newcontent`, `table_preview`, `table_recursive`, `show_dashboard`) VALUES
(2, 0, 'Register', 'register', 'dynamic', 1, '2013-11-21 16:01:06', 1, '2013-11-27 09:41:17', 1, 0, 'asc', 'true', '', 'false', 'false'),
(3, 0, 'Management', 'management', 'static', 2, '2018-12-15 16:20:46', 1, '2018-12-15 16:20:46', 1, 1, 'asc', 'true', '', 'false', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `mother_user`
--

CREATE TABLE `mother_user` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `user_login_name` varchar(50) NOT NULL,
  `user_login_password` varchar(50) NOT NULL,
  `enable_status` enum('enable','disable') NOT NULL DEFAULT 'enable',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mother_user`
--

INSERT INTO `mother_user` (`user_id`, `user_group_id`, `user_login_name`, `user_login_password`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 0, 'developer', '1a460e698f5d598976b3958c297eda1a', 'enable', '2013-11-21 15:50:32', 0, '2013-11-21 15:50:32', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mother_user_group`
--

CREATE TABLE `mother_user_group` (
  `user_group_id` int(11) UNSIGNED NOT NULL,
  `user_group_name` varchar(50) NOT NULL,
  `user_group_permission` longtext NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_management`
--

CREATE TABLE `tbl_management` (
  `management_id` bigint(20) UNSIGNED NOT NULL,
  `mother_shop_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `recursive_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `sort_priority` bigint(20) UNSIGNED NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL DEFAULT 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `promotion` varchar(255) NOT NULL,
  `thankyou_pc` varchar(255) NOT NULL,
  `thankyou_mobile` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_management`
--

INSERT INTO `tbl_management` (`management_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `promotion`, `thankyou_pc`, `thankyou_mobile`) VALUES
(1, 1, 0, 0, 0, 'show', '2018-12-15 16:25:39', 1, '2018-12-15 16:25:39', 1, '/userfiles/images/promotion(1).svg', '/userfiles/images/thankyou.svg', '/userfiles/images/thankyou_mobile.svg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_management_lang`
--

CREATE TABLE `tbl_management_lang` (
  `management_lang_id` bigint(20) UNSIGNED NOT NULL,
  `management_id` bigint(20) UNSIGNED NOT NULL,
  `lang_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_management_lang`
--

INSERT INTO `tbl_management_lang` (`management_lang_id`, `management_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_register`
--

CREATE TABLE `tbl_register` (
  `register_id` bigint(20) UNSIGNED NOT NULL,
  `mother_shop_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `sort_priority` bigint(20) UNSIGNED NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL DEFAULT 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `register_name` varchar(255) NOT NULL,
  `recursive_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `room_type` varchar(255) NOT NULL,
  `price_range` varchar(255) NOT NULL,
  `utm_source` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_register_lang`
--

CREATE TABLE `tbl_register_lang` (
  `register_lang_id` bigint(20) UNSIGNED NOT NULL,
  `register_id` bigint(20) UNSIGNED NOT NULL,
  `lang_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_query`
--
ALTER TABLE `ci_query`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_access`
--
ALTER TABLE `log_access`
  ADD PRIMARY KEY (`access_id`);

--
-- Indexes for table `log_action`
--
ALTER TABLE `log_action`
  ADD PRIMARY KEY (`action_id`);

--
-- Indexes for table `mother_column`
--
ALTER TABLE `mother_column`
  ADD PRIMARY KEY (`column_id`);

--
-- Indexes for table `mother_config`
--
ALTER TABLE `mother_config`
  ADD PRIMARY KEY (`config_id`);

--
-- Indexes for table `mother_config_group`
--
ALTER TABLE `mother_config_group`
  ADD PRIMARY KEY (`config_group_id`);

--
-- Indexes for table `mother_icon`
--
ALTER TABLE `mother_icon`
  ADD PRIMARY KEY (`icon_id`);

--
-- Indexes for table `mother_lang`
--
ALTER TABLE `mother_lang`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `mother_permission`
--
ALTER TABLE `mother_permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `mother_shop`
--
ALTER TABLE `mother_shop`
  ADD PRIMARY KEY (`shop_id`);

--
-- Indexes for table `mother_table`
--
ALTER TABLE `mother_table`
  ADD PRIMARY KEY (`table_id`);

--
-- Indexes for table `mother_user`
--
ALTER TABLE `mother_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `mother_user_group`
--
ALTER TABLE `mother_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `tbl_management`
--
ALTER TABLE `tbl_management`
  ADD PRIMARY KEY (`management_id`);

--
-- Indexes for table `tbl_management_lang`
--
ALTER TABLE `tbl_management_lang`
  ADD PRIMARY KEY (`management_lang_id`);

--
-- Indexes for table `tbl_register`
--
ALTER TABLE `tbl_register`
  ADD PRIMARY KEY (`register_id`);

--
-- Indexes for table `tbl_register_lang`
--
ALTER TABLE `tbl_register_lang`
  ADD PRIMARY KEY (`register_lang_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_query`
--
ALTER TABLE `ci_query`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `log_access`
--
ALTER TABLE `log_access`
  MODIFY `access_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `log_action`
--
ALTER TABLE `log_action`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `mother_column`
--
ALTER TABLE `mother_column`
  MODIFY `column_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `mother_config`
--
ALTER TABLE `mother_config`
  MODIFY `config_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `mother_config_group`
--
ALTER TABLE `mother_config_group`
  MODIFY `config_group_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mother_icon`
--
ALTER TABLE `mother_icon`
  MODIFY `icon_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mother_lang`
--
ALTER TABLE `mother_lang`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mother_permission`
--
ALTER TABLE `mother_permission`
  MODIFY `permission_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mother_shop`
--
ALTER TABLE `mother_shop`
  MODIFY `shop_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mother_table`
--
ALTER TABLE `mother_table`
  MODIFY `table_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mother_user`
--
ALTER TABLE `mother_user`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mother_user_group`
--
ALTER TABLE `mother_user_group`
  MODIFY `user_group_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_management`
--
ALTER TABLE `tbl_management`
  MODIFY `management_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_management_lang`
--
ALTER TABLE `tbl_management_lang`
  MODIFY `management_lang_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_register`
--
ALTER TABLE `tbl_register`
  MODIFY `register_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_register_lang`
--
ALTER TABLE `tbl_register_lang`
  MODIFY `register_lang_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
