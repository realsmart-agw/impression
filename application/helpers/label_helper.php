<?php  
	function label($label){
		$ci = & get_instance();
		$rs = $ci->lang->line($label);
		if($rs)
			return $rs;
		else
			return $label;
	}

	function formatSizeUnits($path)
	{
		$file_path = substr($path, 1);
	    $size = filesize($file_path);
	    $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	    $power = $size > 0 ? floor(log($size, 1024)) : 0;
	    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
	}

	function chkDate($day)
	{
		$date = new DateTime($day);
		$date->modify('+7 day');
		return $date->format('Y/m/d');
	}

	function checkCarectornoDot($data,$length){
		$c = strlen(strip_tags($data)); 
		$count = 0;
	    for ($i = 0; $i < $c; ++$i) if ((ord($data[$i]) & 0xC0) != 0x80) ++$count;

		//$count    = strlen(strip_tags($data));
		return iconv_substr($data,0,$length, "UTF-8");
	}

	function checkCarector($data,$length){
		$c = strlen(strip_tags($data)); 
		$count = 0;
	    for ($i = 0; $i < $c; ++$i) if ((ord($data[$i]) & 0xC0) != 0x80) ++$count;

		//$count    = strlen(strip_tags($data));
		if ($count > $length) {
			return iconv_substr($data,0,$length, "UTF-8").'...';
		}else{
			return iconv_substr($data,0,$length, "UTF-8");
		}
	}

	function utf8_strlen($s) {
		$c = strlen($s); 
		$l = 0;
	    for ($i = 0; $i < $c; ++$i) if ((ord($s[$i]) & 0xC0) != 0x80) ++$l;


	    return $l;
	}
	function dateFormat($date)
	{
		$date = new DateTime($date);
		return $date->format('d / m / Y');
	}