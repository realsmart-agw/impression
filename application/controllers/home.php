<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model("homemodel");
    }

    var $room_type = array('1 BEDROOM','2 BEDROOMS','3 BEDROOMS','DUPLEX');
    var $price = array('8-15 MB.','16-20 MB.','21-25 MB.','26-30 MB.','31-35 MB.','36 MB.+');

    public function index() {
        $data = array();
        $data['page']       = 'home';
        $data['menu']       = 'HOME';
        $data['utm_source'] = isset($_GET['utm_source'])? $_GET['utm_source']: ""; 
        $data['room_type']  = $this->room_type;  
        $data['price']      = $this->price;  
        
        $data['content']    = $this->load->view('pages/home', $data, TRUE);
        $this->load->view('masterpage', $data);
    }

    public function register(){
        // Google reCaptcha secret key
        $secretKey  = "6LeTy40UAAAAADX3NhFBUXA0u7pSkVuN_UXHNp3U";

        $statusMsg = '';
        if(isset($_POST['captcha-response']) && !empty($_POST['captcha-response'])){
            // Get verify response data
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$_POST['captcha-response']);
            $responseData = json_decode($verifyResponse);

            if($responseData->success){
                //Contact form submission code goes here ...
                $statusMsg = 'Your contact request have submitted successfully.';
            }else{
                $statusMsg = 'Robot verification failed, please try again. (captcha not match)';
                // print_r('1:'.$statusMsg);
                redirect('home');
                exit;
            }
        }else{
            $statusMsg = 'Robot verification failed, please try again. (not have captcha)';
            redirect('home');
            // print_r('2:'.$statusMsg);
            exit;
        }
        $shop_id = 1;
        if ( $this->SpamModel->check_spam($_POST['fname'],true,true)
            && $this->SpamModel->check_spam($_POST['lname'],true,true)
            && $this->SpamModel->check_spam($_POST['mobile'],true,true)
            && $this->SpamModel->check_spam($_POST['email'],false,true)
            && $this->SpamModel->check_spam($_POST['nationality'],true,true)
            && $this->SpamModel->check_spam($_POST['roomtype'],true,true)
            && $this->SpamModel->check_spam($_POST['price'],true,true)
            ) {

            $this->db->set('first_name', $_POST['fname']);
            $this->db->set('last_name', $_POST['lname']);
            $this->db->set('email', $_POST['email']);
            $this->db->set('mobile', $_POST['mobile']);
            $this->db->set('room_type', $_POST['roomtype']);
            $this->db->set('price_range', $_POST['price']);
            $this->db->set('nationality', $_POST['nationality']);
            $this->db->set('utm_source', $_POST['utm_source']);

            $this->db->set('parent_id', 0);
            $this->db->set('enable_status', 'show');
            $this->db->set('sort_priority', 0);
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', '0');
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', '0');
            $this->db->set('mother_shop_id', $shop_id);
            $this->db->insert('tbl_register');
            $last_id = $this->db->insert_id();

            $this->db->set('sort_priority', $last_id);
            $this->db->where('register_id', $last_id);
            $this->db->update('tbl_register');

            $mail_subject = 'Register from ' . $this->ConfigModel->getConfig('website_title') . ' : (' . $_POST['email'] . ')';
            $sales_email_message = $this->load->view('emails/register_email', $_POST, TRUE);
            $this->SendMailModel->smtpSalesMail($mail_subject, $sales_email_message);

            // if ($this->ConfigModel->getConfig('sms') != "") {
            //     $number = preg_replace("/[^0-9]/", "", $_POST['mobile']);
            //     $message = $this->ConfigModel->getConfig('sms');

            //     $length = strlen((string) $number);
            //     if ($length == 10) {
            //         $this->SendSMSModel->send($number, $message);
            //     }
            // }
            
            redirect('home/thankyou');
        }
        else {
            redirect('home');
        }
        exit;
    }

     public function thankyou() {
        $data = array();
        $data['page']     = 'thankyou';
        $data['menu']     = 'THANK YOU';

        $data['content']  = $this->load->view('pages/thankyou', $data, TRUE);
        $this->load->view('masterpage', $data);
    }

    public function policy(){
        $data = array();
        $data['page']     = 'poilcy';
        $data['menu']     = 'POLICY';

        $data['content']  = $this->load->view('pages/policy', $data, TRUE);
        $this->load->view('masterpage', $data);
    }

    public function set_language($lang) {
        $this->LanguageModel->set_language($lang);
    }


}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
