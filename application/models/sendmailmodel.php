<?php

class SendMailModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function normalMail($emailto,$subject,$messages, $shop_id) {
        $name = $this->ConfigModel->getSenderName($shop_id);
        $mail = $this->ConfigModel->getSenderFrom($shop_id);

        $header  = "MIME-Version: 1.0" . "\r\n";
        $header  .= "Content-type: text/html; charset=utf-8\n";
        $header  .="From: ".$name." E-mail :".$mail."\r\n";
        $send_mail = mail($emailto,$subject,$messages,$header);

    }

    function normalSalesMail($subject,$messages, $shop_id) {
        $name = $this->ConfigModel->getSenderName($shop_id);
        $mail = $this->ConfigModel->getSenderFrom($shop_id);

        $header  = "MIME-Version: 1.0" . "\r\n";
        $header  .= "Content-type: text/html; charset=utf-8\n";
        $header  .="From: ".$name." E-mail :".$mail."\r\n";

        $receiverArr = $this->ConfigModel->getReceiver($shop_id);
        $to = "";
        if (count($receiverArr) > 0) {
            foreach ($receiverArr as $value) {
                if (trim($value) != '') {
                    if ($to != "") 
                        $to .= ",";
                    $to .= trim($value); 
                }
            }
        }

        $receiverCCArr = $this->ConfigModel->getReceiverCC($shop_id);
        $cc = "";
        if (count($receiverCCArr) > 0) {
            foreach ($receiverCCArr as $value) {
                if (trim($value) != '') {
                    if ($cc != "") 
                        $cc .= ',';
                    $cc .= trim($value);
                }
            }
            if ($cc != "") 
            $header  .= 'Cc: '.$cc . "\r\n";
        }

        $receiverBCCArr = $this->ConfigModel->getReceiverBCC($shop_id);
        $bcc = '';
        if (count($receiverBCCArr) > 0) {
            foreach ($receiverBCCArr as $value) {
                if (trim($value) != '') {
                    if ($bcc != "") 
                        $bcc .= ',';
                    $bcc .= trim($value);
                }
            }
            if ($bcc != "") 
            $header  .= 'Bcc: '.$bcc . "\r\n";
        }

        mail($to,$subject,$messages,$header); 


    }

    function smtpmail($email, $subject, $body, $shop_id) {
        require_once("PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug  = 1;
        $mail->CharSet = "utf-8";  // ในส่วนนี้ ถ้าระบบเราใช้ tis-620 หรือ windows-874 สามารถแก้ไขเปลี่ยนได้                         
        $mail->Host = $this->ConfigModel->getSMTP($shop_id); //  mail server ของเรา
        //$mail->Port = $this->ConfigModel->getPort($shop_id);
        $mail->SMTPAuth = true;     //  เลือกการใช้งานส่งเมล์ แบบ SMTP
        $mail->Username = $this->ConfigModel->getSender($shop_id);   //  account e-mail ของเราที่ต้องการจะส่ง
        $mail->Password = $this->ConfigModel->getSenderPassword($shop_id);  //  รหัสผ่าน e-mail ของเราที่ต้องการจะส่ง

        $mail->From = $this->ConfigModel->getSenderFrom($shop_id);  //  account e-mail ของเราที่ใช้ในการส่งอีเมล
        $mail->FromName = $this->ConfigModel->getSenderName($shop_id); //  ชื่อผู้ส่งที่แสดง เมื่อผู้รับได้รับเมล์ของเรา
        $mail->AddAddress($email);            // Email ปลายทางที่เราต้องการส่ง(ไม่ต้องแก้ไข)
        $mail->IsHTML(true);                  // ถ้า E-mail นี้ มีข้อความในการส่งเป็น tag html ต้องแก้ไข เป็น true
        $mail->Subject = $subject;        // หัวข้อที่จะส่ง(ไม่ต้องแก้ไข)
        $mail->Body = $body;                   // ข้อความ ที่จะส่ง(ไม่ต้องแก้ไข)
        $result = $mail->send();
        return $result;
    }

    function smtpSalesMail($subject, $body) {
        require_once("PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug  = 1;
        $mail->CharSet = "utf-8";  // ในส่วนนี้ ถ้าระบบเราใช้ tis-620 หรือ windows-874 สามารถแก้ไขเปลี่ยนได้                         
        $mail->Host = $this->ConfigModel->getSMTP(); //  mail server ของเรา
        $mail->Port = $this->ConfigModel->getPort();
        $mail->SMTPAuth = true;     //  เลือกการใช้งานส่งเมล์ แบบ SMTP
        $mail->Username = $this->ConfigModel->getSender();   //  account e-mail ของเราที่ต้องการจะส่ง
        $mail->Password = $this->ConfigModel->getSenderPassword();  //  รหัสผ่าน e-mail ของเราที่ต้องการจะส่ง

        $mail->From = $this->ConfigModel->getSenderFrom();  //  account e-mail ของเราที่ใช้ในการส่งอีเมล
        $mail->FromName = $this->ConfigModel->getSenderName(); //  ชื่อผู้ส่งที่แสดง เมื่อผู้รับได้รับเมล์ของเรา
        
        $receiverArr = $this->ConfigModel->getReceiver();
        if (count($receiverArr) > 0) {
            foreach ($receiverArr as $value) {
                if (trim($value) != '') {
                    $mail->AddAddress(trim($value));            // Email ปลายทางที่เราต้องการส่ง(ไม่ต้องแก้ไข)
                }
            }
        }

        $receiverCCArr = $this->ConfigModel->getReceiverCC();
        if (count($receiverCCArr) > 0) {
            foreach ($receiverCCArr as $value) {
                if (trim($value) != '') {
                    $mail->AddCC(trim($value));
                }
            }
        }

        $receiverBCCArr = $this->ConfigModel->getReceiverBCC();
        if (count($receiverBCCArr) > 0) {
            foreach ($receiverBCCArr as $value) {
                if (trim($value) != '') {
                    $mail->AddBCC(trim($value));
                }
            }
        }

        $mail->IsHTML(true);                  // ถ้า E-mail นี้ มีข้อความในการส่งเป็น tag html ต้องแก้ไข เป็น true
        $mail->Subject = $subject;        // หัวข้อที่จะส่ง(ไม่ต้องแก้ไข)
        $mail->Body = $body;                   // ข้อความ ที่จะส่ง(ไม่ต้องแก้ไข)
        $result = $mail->send();
        return $result;
    }

    function smtpdevs($email, $subject, $body) {
        require_once("PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug  = 1;
        $mail->CharSet = "utf-8";
        $mail->Host = $this->ConfigModel->getSMTP();
        $mail->Port = $this->ConfigModel->getPort();

        $mail->SMTPAuth = true;
        $mail->Username = $this->ConfigModel->getSender();
        $mail->Password = $this->ConfigModel->getSenderPassword();

        $mail->From = $this->ConfigModel->getSenderFrom();
        $mail->FromName = $this->ConfigModel->getSenderName();
        $mail->AddAddress($email);
        $mail->IsHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $result = $mail->send();
        return $result;
    }

    function google_smtp_mail($email, $subject, $body, $shop_id) {
        require_once("PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "tls";
        $mail->SMTPDebug  = 1;
        $mail->CharSet = "utf-8";
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 587;
        $mail->Username = "metroprop.condo@gmail.com";
        $mail->Password = "r1r\"!H\ZD{8(rh-UY\C}n";
        $mail->IsHTML(true);

        $mail->From = $this->ConfigModel->getSenderFrom();
        $mail->FromName = $this->ConfigModel->getSenderName();

        $mail->AddAddress($email);

        $mail->Subject = $subject;
        $mail->Body = $body;

        $result = $mail->send();
        return $result;
    }

    function google_smtp_mail_sale($subject, $body, $shop_id) {
        require_once("PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "tls";
        $mail->SMTPDebug  = 1;
        $mail->CharSet = "utf-8";
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 587;

        $mail->Username = "metroprop.condo@gmail.com";
        $mail->Password = "r1r\"!H\ZD{8(rh-UY\C}n";

        $mail->IsHTML(true);

        $mail->From = $this->ConfigModel->getSenderFrom();
        $mail->FromName = $this->ConfigModel->getSenderName();
        
        $receiverArr = $this->ConfigModel->getReceiver($shop_id);
        if (count($receiverArr) > 0) {
            foreach ($receiverArr as $value) {
                if (trim($value) != '') {
                    $mail->AddAddress(trim($value));            // Email ปลายทางที่เราต้องการส่ง(ไม่ต้องแก้ไข)
                }
            }
        }

        $receiverCCArr = $this->ConfigModel->getReceiverCC($shop_id);
        if (count($receiverCCArr) > 0) {
            foreach ($receiverCCArr as $value) {
                if (trim($value) != '') {
                    $mail->AddCC(trim($value));
                }
            }
        }

        $receiverBCCArr = $this->ConfigModel->getReceiverBCC($shop_id);
        if (count($receiverBCCArr) > 0) {
            foreach ($receiverBCCArr as $value) {
                if (trim($value) != '') {
                    $mail->AddBCC(trim($value));
                }
            }
        }

        $mail->IsHTML(true);                  // ถ้า E-mail นี้ มีข้อความในการส่งเป็น tag html ต้องแก้ไข เป็น true
        $mail->Subject = $subject;        // หัวข้อที่จะส่ง(ไม่ต้องแก้ไข)
        $mail->Body = $body;                   // ข้อความ ที่จะส่ง(ไม่ต้องแก้ไข)
        $result = $mail->send();
        return $result;
    }

}
