<?php

class SpamModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function strposa($haystack, $needle, $offset=0) {
        if ($haystack != "") {
            if(!is_array($needle)) $needle = array($needle);
            foreach($needle as $query) {
                if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
            }
        }
        return false;
    }

    function check_spam($postStr, $validate = false, $require = false) {
        $returnVal = false;
        $arrvalidate = array("http://","https://","=","~","!","@","#","%","&","*","(",")","_","<",">","?","{","}","$","^","'",'"',":",";");

        if ($require == true) {
            if ($validate == true) {
                //firstname,lastname,mobile
                if(trim($postStr) != "" || !$this->strposa($postStr, $arrvalidate, 1)){
                    $returnVal = true;
                    //echo 'ไม่เท่ากับค่าว่าง : '.str_replace($arrvalidate, "", $postStr).'<br>';
                }
            }else{
                //email
                if(trim($postStr) != "" || !filter_var($postStr, FILTER_VALIDATE_EMAIL)){
                    $returnVal = true;
                }
            }
        }else{
            if ($postStr != strip_tags($postStr) || !$this->strposa($postStr, $arrvalidate, 1)) {
                //Message
                $returnVal = true;
            }
        }
        
        //if return true equal spam
        //if return false equal not spam
        return $returnVal;
    }

}

?>
