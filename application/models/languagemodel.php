<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of languagemodel
 *
 * @author tomozard
 */
class LanguageModel extends CI_Model {

    private $default_lang = 'en';

    function __construct() {
        parent::__construct();
    }

    function get_language() {
        $this->session->set_userdata('lang', $this->default_lang);
        if ($this->session->userdata('lang')) {
            return $this->session->userdata('lang');
        } else {
            $this->session->set_userdata('lang', $this->default_lang);
            $langs  = $this->getLangName($this->default_lang);
            $this->session->set_userdata('language', $langs->lang_name);
            $this->session->set_userdata('lang_id', $langs->lang_id);
            return $this->session->userdata('lang');
        }
    }

    function set_language($lang) {
        $this->session->set_userdata('lang', $lang);
        $langs  = $this->getLangName($lang);
        $this->session->set_userdata('language', $langs->lang_name);
        $this->session->set_userdata('lang_id', $langs->lang_id);

        $url = base_url();
        if ($this->agent->is_referral()) {
            $url = $this->agent->referrer();
        }
        redirect($url);
    }

    function getLangName($lang_code) {
        $this->db->where('lang_code', $lang_code);
        $query = $this->db->get('mother_lang');
        return $query->row();
    }

    function getLang() {
        $this->db->order_by('sort_priority');
        return $this->db->get('mother_lang');
    }




}

?>
