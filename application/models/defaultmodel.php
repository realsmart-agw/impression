<?php

class DefaultModel extends CI_Model {

    function __construct() {
		parent::__construct();

        $this->load->model("LanguageModel");
        $this->LanguageModel->get_language();
        if($this->session->userdata('lang') == 'th')
            $this->lang->load('thai', 'thai');
        else
            $this->lang->load('english', 'english');
    }

    function getManagement() {
        $this->db->group_by('a.management_id');
        
        $this->db->join('tbl_management_lang al', 'al.management_id = a.management_id');
        $this->db->where('al.lang_id',$this->session->userdata('lang_id'));
        return $this->db->get('tbl_management a')->row();
    }

    function getListGroup() {
        $this->db->group_by('a.list_group_id');
        
        $this->db->join('tbl_list_group_lang al', 'al.list_group_id = a.list_group_id');
        $this->db->where('al.lang_id',$this->session->userdata('lang_id'));
        $this->db->where('a.enable_status','show');
        return $this->db->get('tbl_list_group a');
    }

}

?>
