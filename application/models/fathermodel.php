<?php

class FatherModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function queryShop() {
        $this->db->order_by('sort_priority');
        return $this->db->get('mother_shop');
    }

    function getDefaultShop() {
        $this->db->limit(1, 0);
        $this->db->order_by('sort_priority', 'ASC');
        $query = $this->db->get('mother_shop');
        return $query->row();
    }

    function getShopName($shop_id) {
        $this->db->select('shop_name');
        $this->db->where('shop_id', $shop_id);
        $query = $this->db->get('mother_shop');
        $row = $query->row();
        return $row->shop_name;
    }

    function getShopCode($shop_id) {
        $this->db->select('shop_code');
        $this->db->where('shop_id', $shop_id);
        $query = $this->db->get('mother_shop');
        $row = $query->row();
        return $row->shop_code;
    }

    function getShopId($shop_code) {
        $this->db->select('shop_id');
        $this->db->where('shop_code', $shop_code);
        $query = $this->db->get('mother_shop');
        $row = $query->row();
        return $row->shop_id;
    }

    function getContentList($mother_shop_id = 0, $table_code = 0, $parent_id = 0, $per_page = 0, $page = 1, $query_array = array(), $recursive_id = 0) {
        if ($mother_shop_id == 0) {
            $mother_shop_id = $this->session->userdata('mother_shop_id');
        }
        $lang_id = $this->getLangId($this->LanguageModel->get_language());
        //$table_code = $this->getTableCode($table_id);
        //getter query_array
        $strWhere = "";
        if (count($query_array) > 0) {
            $getSearchColumnList = $this->StructureModel->getSearchColumnList($table_id);
            if ($getSearchColumnList->num_rows() > 0) {
                foreach ($getSearchColumnList->result_array() AS $row) {
                    if ($row['column_lang'] > 0) {
                        $queryLang = $this->LangModel->queryLangName();
                        foreach ($queryLang->result_array() AS $row2) {
                            $strWhere .= $this->FieldTypeModel->getFieldSearch($row, $row2, 'tbl_' . $table_code . '.');
                        }
                    } else {
                        $strWhere .= $this->FieldTypeModel->getFieldSearch($row, null, 'tbl_' . $table_code . '.');
                    }
                }
            }
        }

        $this->db->select('tbl_' . $table_code . '_lang.*');
        $this->db->select('tbl_' . $table_code . '.*');
        $this->db->join('tbl_' . $table_code . '_lang', 'tbl_' . $table_code . '_lang.' . $table_code . '_id = tbl_' . $table_code . '.' . $table_code . '_id AND lang_id = ' . $lang_id, 'left');
        $this->db->where('tbl_' . $table_code . '.enable_status !=', 'delete');
        $this->db->where('tbl_' . $table_code . '.mother_shop_id', $mother_shop_id);
        $this->db->where('recursive_id', $recursive_id);
        if ($parent_id != 0) {
            $this->db->where('parent_id', $parent_id);
        }

        if ($strWhere != '') {
            $this->db->where(substr($strWhere, 4), NULL, FALSE);
        }

        $this->db->order_by('tbl_' . $table_code . '.sort_priority asc, tbl_' . $table_code . '.' . $table_code . '_id asc');
        if ($per_page > 0) {
            //check per page if $per_page == 0 is unlimit
            $this->db->limit($per_page, ($page * $per_page) - $per_page);
        }
        $query = $this->db->get('tbl_' . $table_code);
        return $query;
    }

    function getLangId($lang_code) {
        $this->db->where('lang_code', $lang_code);
        $query = $this->db->get('mother_lang');
        return $query->row()->lang_id;
    }
    function getOrder($table_code) {
        $this->db->select('table_order');
        $this->db->where('table_code', $table_code);
        $query = $this->db->get('mother_table');
        $row = $query->row();
        return $row->table_order;
    }

}

?>
