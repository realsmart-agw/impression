<?php

class ConfigModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getConfig($config_code = '') {
        $this->db->where('config_code', $config_code);
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    //Email Setting
    function getSender() {
        $this->db->where('config_code', 'email_account');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getSenderFrom() {
        $this->db->where('config_code', 'from');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getSenderName() {
        $this->db->where('config_code', 'from_name');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getSenderPassword() {
        $this->db->where('config_code', 'password');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getSMTP() {
        $this->db->where('config_code', 'smtp');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getPort() {
        $this->db->where('config_code', 'port');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getReceiver() {
        //return Array
        $this->db->where('config_code', 'to');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        $value = explode(",", $row->config_value);
        return $value;
    }

    function getReceiverCC() {
        //return Array
        $this->db->where('config_code', 'cc');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        $value = explode(",", $row->config_value);
        return $value;
    }

    function getReceiverBCC() {
        //return Array
        $this->db->where('config_code', 'bcc');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        $value = explode(",", $row->config_value);
        return $value;
    }

    function getManagement($name = ''){
        $query = $this->db->get('tbl_management');
        $row = $query->row();
        return $row->$name;
    }

}

?>
