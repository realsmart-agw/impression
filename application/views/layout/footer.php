<div id="footer">
	<div class="flexPC">
		<a href="https://www.facebook.com/AllInspire/" target="_blank">
			<img src="<?php echo base_url('assets/imgs/icon_fb.svg');?>" class="social">
		</a>
		<a href="tel:020299999" class="tel">02 029 9999</a>
		<div class="policy">
			2018 © AHJ Ekkamai Company Limited All Rights Reserved.
			<a href="<?php echo site_url('home/policy')?>">Privacy Policy</a>
		</div>
	</div>
	<div class="company">
		<a href="https://www.allinspire.co.th" target="_blank"><img src="<?php echo base_url('assets/imgs/logo_footer_03.svg');?>"></a>
		<a href="javascript:void(0)" target="_blank"><img src="<?php echo base_url('assets/imgs/logo_footer_01.svg');?>"></a>
		<a href="javascript:void(0)" target="_blank"><img src="<?php echo base_url('assets/imgs/logo_footer_02.svg');?>"></a>
	</div>
	<div class="hidden-md hidden-lg" id="tabbar">
		<div class="tabbar">
			<a href="tel:020299999" class="tabbar__item">
				<img src="<?php echo base_url('assets/imgs/icon_call.svg');?>" class="tabbar__item--img">
				CALL
			</a>
			<div class="tabbar__border"></div>
			<a href="https://www.google.com/maps/dir/Current+Location/<?php echo $this->ConfigModel->getConfig('google_map'); ?>" target="_blank" class="tabbar__item">
				<img src="<?php echo base_url('assets/imgs/icon_direction.svg');?>" class="tabbar__item--img">
				DIRECTION
			</a>
			<div class="tabbar__border"></div>
			<a href="javascript:void(0)" class="tabbar__item" id="goRegister">
				<img src="<?php echo base_url('assets/imgs/icon_register.svg');?>" class="tabbar__item--img">
				REGISTER
			</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#goRegister').click(function(event) {
		$('input[name="fname"]').focus();

		// if($( window ).width() < 992){
		// 	$('html,body').animate({
		// 		scrollTop: $(".register").offset().top
		// 	},'slow');
		// }
	});
	$(function() {
		$(window).scroll(function() {
			var scrollss = $(this).scrollTop();

			if(scrollss > 100){
				$('#tabbar').fadeIn();
				$('.tabbar').removeClass('out');
			}else{
				$('#tabbar').fadeOut();
				$('.tabbar').addClass('out');
			}
		});
	});
</script>