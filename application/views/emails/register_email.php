<style>
    html, body { margin: 0; padding: 0; }
</style>

<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="background-color: #aeaeae;">
    <tbody>
        <tr>
            <td>
                <table width="640" cellpadding="0" cellspacing="0" border="0" align="center">
                    <thead>
                        <tr>
                            <td style="border:none; border-top:3px solid #0c0a0d; background-color:#fff; padding: 30px;">
                                <a href="<?php echo site_url(); ?>" target="_blank" style="display:block; text-align:center; border:none; outline:0;">
                                    <img src="<?php echo base_url('assets/imgs/logo.png'); ?>"  />
                                </a>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="border:none; background-color:#fff; padding: 30px;">
                                <p style="color: #456;font-family: helvetica, arial, sans-serif;font-size: 18px;line-height: 1.4;padding: 0;">
                                <div style="padding-bottom:5px;">
                                    <label style="width:200px;display:inline-block;">FullName</label>
                                    <span><?php echo ': ' .$fname.' '.$lname; ?></span>
                                </div>
                                <div style="padding-bottom:5px;">
                                    <label style="width:200px;display:inline-block;">Email</label>
                                    <span><?php echo ': ' . $email; ?></span>
                                </div>
                                <div style="padding-bottom:5px;">
                                    <label style="width:200px;display:inline-block;">Mobile</label>
                                    <span><?php echo ': ' .$mobile; ?></span>
                                </div>
                                <div style="padding-bottom:5px;">
                                    <label style="width:200px;display:inline-block;">Room Type</label>
                                    <span><?php echo ': ' .$roomtype; ?></span>
                                </div>
                                <div style="padding-bottom:5px;">
                                    <label style="width:200px;display:inline-block;">Price Range</label>
                                    <span><?php echo ': ' .$price; ?></span>
                                </div>
                                <?php if ($utm_source != ""): ?>
                                <div style="padding-bottom:5px;">
                                    <label style="width:200px;display:inline-block;">Reference</label>
                                    <span><?php echo ': ' .$utm_source; ?></span>
                                </div>
                                <?php endif ?>
                                </p>
                            </td>
                        </tr>
                    </tbody>
     
                </table>
            </td>
        </tr>
    </tbody>
</table>
