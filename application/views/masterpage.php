<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="<?php echo $this->ConfigModel->getConfig('meta_description'); ?>">
    <meta name="keyword" content="<?php echo $this->ConfigModel->getConfig('meta_keywords'); ?>">
    <meta name="author" content="">

    <meta property="og:title" content="<?php echo $this->ConfigModel->getConfig('og_title'); ?>">
    <meta property="og:description" content="<?php echo $this->ConfigModel->getConfig('og_description'); ?>">
    <meta property="og:site_name" content="<?php echo $this->ConfigModel->getConfig('og_sitename'); ?>">
    <meta property="og:type" content="<?php echo $this->ConfigModel->getConfig('og_type'); ?>">
    <meta property="og:url" content="<?php echo $this->ConfigModel->getConfig('og_url'); ?>">
    <meta property="og:image" content="<?php echo $this->ConfigModel->getConfig('og_image'); ?>">
    
    <title><?php echo $this->ConfigModel->getConfig('website_title'); ?></title>
    <link rel="icon" href="<?php echo $this->ConfigModel->getConfig('favicon'); ?>">

    <link href="<?php echo base_url('assets/css/min/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/min/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/min/validationEngine.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/min/easydropdown.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/min/animate.css'); ?>" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url('assets/js/min/ie10-viewport-bug-workaround.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/min/ie-emulation-modes-warning.js'); ?>"></script>
    

    <?php echo $this->ConfigModel->getConfig('google_analytic'); ?>

    <!-- Google CDN jQuery with fallback to local -->
    <script src="<?php echo base_url('assets/js/min/jquery-1.11.1.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/min/bootstrap.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/min/modernizr.custom.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/min/jquery.form-validator.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/min/background.cycle.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/min/jquery.easydropdown.js'); ?>"></script>

    <!-- annimation -->
    <link href="<?php echo base_url('assets/css/aos.css'); ?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/js/min/aos.js'); ?>"></script>

    <!-- font  Open+Sans-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,400" rel="stylesheet">
</head>
<body>
    <?php echo $this->ConfigModel->getConfig('google_remarketing'); ?>
    <div class="loading">
        <img src="<?php echo base_url('assets/imgs/logo_white.svg');?>">
    </div> 
    <div id="main">
        <?php echo $this->load->view('layout/header', array(), TRUE); ?>
        <?php echo $content; ?>
        <?php echo $this->load->view('layout/footer', array(), TRUE); ?>
    </div>
    <script>
        $( window ).load(function() {
            $('.loading').delay( 1000 ).fadeOut(700);
            AOS.init();
            $('input, textarea').focusout(function() {
                if ($(this).val().length > 0) {
                    $(this).addClass('input');
                }else {
                    $(this).removeClass('input');
                }
            });

            $('input, textarea').each(function(){ 
                if ($(this).val().length > 0) {
                    $(this).addClass('input');
                }else {
                    $(this).removeClass('input');
                }
            });

            $.validate({
                validateOnEvent: false,
                borderColorOnError : '#f44336',
            });

            $( "#frm_register" ).submit(function( event ) {
                if ($( "#frm_register" ).find(".error").hasClass( "error" ) == false) {
                    $('input[type="submit"]').attr('disabled',true).css('opacity','0.5')
                    $('input[type="image"]').attr('disabled',true).css('opacity','0.2')
                }                
            });

        });

        // function changeColor(type){
        //     $('#color'+type+' .selected').css({
        //         'color':'#000000',
        //         'border-bottom':'1px solid #d2ac69',
        //     })
        //     $('#error'+type).hide()
        // }

    </script>
</body>
</html>