<?php 
	$company_en = "AHJ Ekkamai Company Limited.";
?>
<div id="policy">
	<h1>Privacy Policy</h1>
	<div class="detail">
		<?php echo $company_en;?> referred to as &ldquo;The Company&rdquo;, created this website with the commitment to protect personal information.<br class="hidden-xs hidden-sm"> You can visit our website anytime you want without giving any information about yourself. However, in some cases personal<br class="hidden-xs hidden-sm"> information may be required in order for us to provide the services you request.
	</div>

	<h2>Personal Information Collection</h2>
	<div class="detail">
		You will be asked to provide your personal information to facilitate individual correspondence. To receive updates, information<br class="hidden-xs hidden-sm"> about us or special offers registration is required first. The data we collect includes your name, address, phone, email,<br class="hidden-xs hidden-sm"> and other information you give us when you request services from us.
		<p>
			We will also collect certain information related to the software and hardware of your computer including your IP address,<br class="hidden-xs hidden-sm"> browser type, operating system, domain name, access times and referring website addresses. Such information is used to facilitate<br class="hidden-xs hidden-sm"> service operation and maintain service quality.</p>
	</div>

	<h2>Personal Information Control System</h2>
	<div class="detail">
		When you finish registration, <a href="<?php echo site_url();?>" target="_blank"><?php echo site_url();?></a> will keep all data as confidential except in the case of legal investigation<br class="hidden-xs hidden-sm"> or when it is necessary to protect your right or the Company&rsquo;s assets. The registration also helps us understand your needs in order to improve our services<br class="hidden-xs hidden-sm"> and web communications.
	</div>

	<h2>Use of Cookies</h2>
	<div class="detail">
		Every time you visit the website, cookies will be stored on your computer (for visitors whose computer accepts cookies). <br class="hidden-xs hidden-sm">Cookies maintain data during your visit to the site and contain information about this visit. On this site we use cookies to gather visitor statistics.
	</div>

	<div>
		<a href="<?php echo site_url();?>" class="button">
			GO BACK TO REGISTRATION PAGE
    	</a>
	</div>
</div>