<div id="thankyou">
	<div class="banner">
		<div>
			<img src="<?php echo $this->ConfigModel->getManagement('thankyou_pc');?>" class="hidden-xs hidden-sm">
			<img src="<?php echo $this->ConfigModel->getManagement('thankyou_mobile');?>" class="hidden-md hidden-lg">
			<div><a href="https://www.allinspire.co.th" target="_blank" class="button">GO TO ALLINSPIRE.CO.TH</a></div>
		</div>
	</div>
</div>
<?php echo $this->load->view('pages/location', array(), TRUE); ?>