<div id="home">
	<div class="banner">
		<div class="promotion">
			<img src="<?php echo $this->ConfigModel->getManagement('promotion');?>" id="promotion">
		</div>
		<div class="register">
			<div class="clear"></div>
			<?php echo form_open('home/register', array('id' => 'frm_register', 'class' => 'form', 'role' => 'form')); ?>
				<input type="hidden" name="utm_source" value="<?php echo $utm_source ?>" />
				<div class="col-xs-12 col-md-6">
	                <div class="formgroup first-child">
	                    <input type="text" id="FirstName" name="fname" data-validation="length" data-validation-length="min1" data-validation-error-msg="Please Enter First Name" />
	                    <span class="highlight"></span>
	                    <span class="bar"></span>
	                    <label for="FirstName">FIRST NAME *</label>
	                </div>
	            </div>
	            <div class="col-xs-12 col-md-6">
	                <div class="formgroup last-child">
	                    <input type="text" id="LastName" name="lname" data-validation="length" data-validation-length="min1" data-validation-error-msg="Please Enter Last Name" />
	                    <span class="highlight"></span>
	                    <span class="bar"></span>
	                    <label for="LastName">LAST NAME *</label>
	                </div>
	            </div>

	            <div class="col-xs-12 col-md-6">
	                <div class="formgroup last-child">
	                    <input type="text" id="Email" name="email" data-validation="email" data-validation-error-msg="Please Enter Email" />
	                    <span class="highlight"></span>
	                    <span class="bar"></span>
	                    <label for="Email">EMAIL *</label>
	                </div>
	            </div>

	            <div class="col-xs-12 col-md-6">
	                <div class="formgroup first-child">
	                    <input type="text" id="Mobile" name="mobile" data-validation="number" data-validation-allowing="float" maxlength="15"  data-validation-error-msg="Please Enter Phone Number"/>
	                    <span class="highlight"></span>
	                    <span class="bar"></span>
	                    <label for="Mobile">MOBILE *</label>
	                </div>
	            </div>
	            <div class="col-xs-12">
	            	<div class="radiogroup__title">NATIONALITY *
	            		<div class="has-error" for="nationality">Please select nationality</div>
	            	</div>
	            	<div class="radiogroup">
	            		<label class="radiogroup__roomtype">
						    <input type="radio" class="radiogroup" name="nationality" 
						    		value="thai"/>
						    THAI
            			</label>
            			<label class="radiogroup__roomtype">
						    <input type="radio" class="radiogroup" name="nationality" 
						    		value="foreigner"/>
						    FOREIGNER
            			</label>
	            	</div>
	            </div>
	            <div class="col-xs-12">
	            	<div class="radiogroup__title">ROOM TYPE VPREFERENCE *
	            		<div class="has-error" for="roomtype">Please select room type</div>
	            	</div>
	            	<div class="radiogroup">
	            		<?php foreach ($room_type as $key => $value) {?>
	            			<label class="radiogroup__roomtype">
							    <input type="radio" class="radiogroup" name="roomtype" 
							    		value="<?php echo $value;?>"/>
							    <?php echo $value;?>
	            			</label>
	            		<?php } ?>
	            	</div>
	            </div>
	            <div class="col-xs-12">
	            	<div class="radiogroup__title">PRICE RANGE PREFERENCE *
	            		<div class="has-error" for="price">Please select price range</div>
	            	</div>
	            	<div class="radiogroup">
	            		<?php foreach ($price as $key => $value) {?>
	            			<label class="radiogroup__price">
							    <input type="radio" class="radio" name="price" 
							    		value="<?php echo $value;?>" />
							    <?php echo $value;?>
	            			</label>
	            		<?php } ?>
	            	</div>
	            </div>
	            <input type="hidden" name="captcha-response" value="" />
	            <div class="col-xs-12 col-md-8 register__remark">* IMPORTANT INFO, PLEASE FILL IN CORRECTLY</div>
	            <div class="col-xs-12 col-md-4">
	            	<input id="submit" type="submit" class="submit" value="SUBMIT" />
	            </div>
			<?php echo form_close(); ?>
			<div class="clear"></div>
		</div>
	</div>
</div>
<?php echo $this->load->view('pages/location', array(), TRUE); ?>

<script type="text/javascript">
	$('#promotion').click(function(event) {
		$('input[name="fname"]').focus();
	});
	$("input[name='roomtype']").change(function() {
	    checkinput();
	});
	$("input[name='price']").change(function() {
	    checkinput();
	});
	$('#frm_register').on('submit', function(e) {
		var valid = checkinput();
		if (!valid) {
			e.preventDefault();
		}
	});
	function checkinput(){
		var nationality = $("input[name='nationality']:checked").val();
		var roomtype = $("input[name='roomtype']:checked").val();
		var price = $("input[name='price']:checked").val();

		if(nationality == '' ||
			nationality == undefined){
			$(".has-error[for='nationality']").css('opacity','1');
		}else{
			$(".has-error[for='nationality']").css('opacity','0');
		}

		if(roomtype == '' ||
			roomtype == undefined){
			$(".has-error[for='roomtype']").css('opacity','1');
		}else{
			$(".has-error[for='roomtype']").css('opacity','0');
		}

		if(price == '' ||
			price == undefined){
			$(".has-error[for='price']").css('opacity','1');
		}else{
			$(".has-error[for='price']").css('opacity','0');
		}

		if (nationality == '' ||
			nationality == undefined ||
			roomtype == '' ||
			roomtype == undefined ||
			price == '' ||
			price == undefined) {
			$('#submit').prop('disabled', true);
			$('#submit').css('opacity','0.5');
			$('#submit').removeClass('bgcolor');
			return false;
		}else{
			$('#submit').prop('disabled', false);
			$('#submit').css('opacity','1');
			$('#submit').addClass('bgcolor');
			return true;
		}
	}

	function reCAPTCHA_execute () {
	    // reCAPTCHA_site_key comes from backend
	    grecaptcha.execute('6LeTy40UAAAAAJ6eA2ORAvcQJFEPA5xyrXz5nTbh', { action: 'homepage' }).then(function (token) {
	      $('[name="captcha-response"]').val(token);
	      console.log('token : ', token);
	    }, function (reason) {
	      console.log(reason);
	    });
	}
</script>
<style type="text/css">
    .grecaptcha-badge,.g-recaptcha{
        display: none;
    }
</style>
<script src="https://www.google.com/recaptcha/api.js?render=6LeTy40UAAAAAJ6eA2ORAvcQJFEPA5xyrXz5nTbh&onload=reCAPTCHA_execute" async defer></script>
