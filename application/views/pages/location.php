<div id="location">
	<div class="header" data-aos="fade-down">
		<div class="header__title">LOCATION</div>
		<div class="header__control">
			<div class="header__control--button active" id="graphic_map">GRAPHIC MAP</div>
			<div class="header__control--button" id="google_map">COORDINATE</div>
		</div>
	</div>
	<div class="graphic_map" id="graphicMap" data-aos="flip-down" data-aos-anchor-placement="center-bottom">
		<div class="graphic_map__close"><img src="<?php echo base_url('assets/imgs/close.svg');?>"></div>
		<div class="zoom__img"><img src="<?php echo base_url('assets/imgs/graphic_map.jpg');?>"></div>
	</div>
	<div class="zoom_map hidden-md hidden-lg" data-aos="fade-down">
		<img src="<?php echo base_url('assets/imgs/zoom.svg');?>">
		View Large Map
	</div>
	<div class="google_map" id="googleMap"></div>
	<div class="contact" data-aos="fade-down">
		<div class="contact__address">
			Located in luxurious and exclusive lifestyles<br class="hidden-xs hidden-sm">
			Only 2 mins from BTS Ekkamai
			<br><br>
			Only 5 mins to Emporium And Emquatier,<br class="hidden-xs hidden-sm">
			the large Shopping mall in Sukhumvit Bangkok

		</div>
		<div class="contact__direction">
			<a href="https://www.google.com/maps/dir/Current+Location/<?php echo $this->ConfigModel->getConfig('google_map'); ?>" target="_blank"><div class="contact__direction--button">GET DIRECTION</div></a>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#graphic_map').click(function(event) {
		$('.header__control--button').removeClass('active');
		$(this).addClass('active');
		$('#graphicMap').show();
		$('.zoom_map').show();
		$('#googleMap').hide();
	});

	$('#google_map').click(function(event) {
		$('.header__control--button').removeClass('active');
		$(this).addClass('active');
		$('#googleMap').show();
		$('#graphicMap').hide();
		$('.zoom_map').hide();
	});

	$('.zoom_map').click(function(event) {
		$('#graphicMap').addClass('zoom');
    	$('#graphicMap').scrollLeft($(document).height());
	});
	$('#graphicMap').click(function(event) {
		$('#graphicMap').removeClass('zoom');
	});
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->ConfigModel->getConfig('google_map_api_key'); ?>&language=en&callback=initialize"></script>

<script type="text/javascript">
function initialize() {
  var latlng = new google.maps.LatLng(<?php echo $this->ConfigModel->getConfig('google_map'); ?>);
  var mapProp =  {
            center:latlng,
            zoom:16,
            scaleControl: false,
            streetViewControl: true,
            mapTypeControl: false,
            zoomControl: true,
        };
  var map = new google.maps.Map(document.getElementById('googleMap'), mapProp);

  var marker = new google.maps.Marker({
  	position: latlng,
  	map: map,
  	animation: google.maps.Animation.DROP
  });
}
</script>
