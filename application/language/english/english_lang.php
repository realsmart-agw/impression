<?php 
$lang['FirstName'] = "FIRST NAME";
$lang['LastName'] = "LAST NAME";
$lang['Email'] = "EMAIL";
$lang['Telephone'] = "PHONE NUMBER";
$lang['Message'] = "MESSAGE";
	$lang['errorFirstName'] = "Please Enter First Name";
	$lang['errorLastName'] 	= "Please Enter Last Name";
	$lang['errorEmail'] 	= "Please Enter Email";
	$lang['errorTelephone'] = "Please Enter Phone Number";
	$lang['errorMessage'] 	= "Please don't enter special character";

// HOME
$lang['Property_Highlights'] = "Property Highlights";
$lang['Subscribe_Now'] 	   = "Subscribe Now";
$lang['Get_updated'] 	   = "Get updated with our latest news. Just enter your email.";
$lang['Enter_email'] 	   = "Enter email here";
$lang['Subscribe_success'] = "Thank you, you had subscribe successfully.";
$lang['Subscribe_already'] = "We found this email had already subscribed in this website.";

// FOOTER
$lang['List_Group'] 	 = "List Group";

$lang['SERVICE'] 		 = "SERVICE";
$lang['PROPERTY'] 		 = "PROPERTY";
$lang['Contact_us'] 	 = "Contact us";

$lang['contact_success'] = "contact_success";
$lang['Contact_Now'] 	 = "Contact Now";
$lang['Send'] 			 = "Send";
$lang['Share'] 			 = "Share";
$lang['NEWS'] 			 = "NEWS";


$lang[''] 				 = "";
$lang[''] 				 = "";
$lang[''] 				 = "";
$lang[''] 				 = "";
$lang[''] 				 = "";
?>