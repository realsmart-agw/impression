var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');


gulp.task('topsass', function () {
  return gulp.src('assets/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('assets/css/min'));
});

gulp.task('sass', function () {
  return gulp.src('assets/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('assets/css/min'));
});

gulp.task('sass-minicss' ,function() {
    return gulp
        .src('assets/css/min/style.css')
        .pipe(minifyCSS())
        .pipe(gulp.dest('assets/css/min')); 
});

gulp.task('minicss' ,function() {
    return gulp
        .src(['assets/css/animate.css','assets/css/easydropdown.css'])
        .pipe(minifyCSS())
        .pipe(gulp.dest('assets/css/min')); 
});

gulp.task('minijs', function() {
    return gulp.src('assets/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('assets/js/min'));
});

gulp.task('mini-imgs', function(cb) {
    gulp.src(['assets/imgs/*.png','assets/imgs/*.jpg','assets/imgs/**/*.png','assets/imgs/**/*.jpg'])
    .pipe(imagemin())
    .pipe(gulp.dest('assets/imgs'))
});

gulp.task('mini-userfile', function(cb) {
    gulp.src(['userfiles/images/*.png','userfiles/images/*.jpg'])
    .pipe(imagemin())
    .pipe(gulp.dest('userfiles/images'))
});

gulp.task('watch', function () {
  gulp.watch('assets/js/*.js', ['minijs']);
  gulp.watch('assets/css/*.css', ['minicss']);
  //gulp.watch(['assets/imgs/*.png','assets/imgs/*.jpg','assets/imgs/**/*.png','assets/imgs/**/*.jpg'], ['mini-imgs']);
  //gulp.watch(['userfiles/images/*.png','userfiles/images/*.jpg'], ['mini-userfile']);
  gulp.watch('assets/sass/*.scss', ['topsass']);
  gulp.watch('assets/sass/**/*.scss', ['sass']);
  gulp.watch('assets/css/min/style.css', ['sass-minicss']);
  
});